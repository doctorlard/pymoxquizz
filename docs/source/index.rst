.. PyMoxQuizz documentation master file, created by
   sphinx-quickstart on Mon Feb  2 14:28:45 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to PyMoxQuizz's documentation!
======================================

Contents:

.. toctree::
   :maxdepth: 2

.. automodule:: moxquizz.quiz
   :members:

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

