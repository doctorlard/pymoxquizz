#!/usr/bin/env python
## -*- coding: utf-8 -*-
"""
A MozQuizz question library for Python.
See http://moxquizz.de/ for the original implementation in TCL.
"""

from __future__ import unicode_literals, print_function
