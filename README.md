PyMoxQuizz
==========

A MozQuizz question library for Python. See http://moxquizz.de/ for the original
implementation in TCL.

Documentation for this library can be built with Sphinx, or you can read it
online at http://pymoxquizz.readthedocs.org/
